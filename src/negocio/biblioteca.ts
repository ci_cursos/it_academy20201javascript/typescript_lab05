import {LivroRepositorio} from '../persistencia/livroRepositorio';
import {Livro} from '../entidades/livro';
import {Emprestimo} from '../entidades/emprestimo';
import { EmprestimoRepositorio } from '../persistencia/emprestimoRepositorio';

export class Biblioteca {
    static async buscarTodosLivros(): Promise<Livro[]> {
        return LivroRepositorio.buscar();
    }
    static async emprestarLivro(id: string): Promise<Emprestimo> {
        const livro = await LivroRepositorio.buscarPorId(id);
        if (livro !== null) {
            let emprestimo: Emprestimo = {
                livro: livro,
                dataRetirada: new Date(Date.now()),
                dataEntrega: new Date(Date.now()+7*24*60*60*1000)
            };
            return await EmprestimoRepositorio.criar(emprestimo);
        } else {
            throw new Error('Id não existente');
        }
    }
}