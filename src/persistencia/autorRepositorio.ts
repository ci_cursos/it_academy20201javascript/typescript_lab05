import { Autor } from "../entidades/autor";
import { AutorModel } from "./autorModel";

export class AutorRepositorio {
    static async criar(autor: Autor): Promise<Autor> {
        return AutorModel.create(autor); //retorna uma Promise
    }

    static async buscar(): Promise<Autor[]> {
        let consulta = AutorModel.find();
        return consulta.exec(); //retorna uma Promise
    }

    static async buscarPorPrimeiroNome(nome: string): Promise<Autor[]> {
        let consulta = AutorModel.find().where('primeiro_nome').equals(nome);
        return consulta.exec();
    }

    static async alterar(autor: Autor, id: string): Promise<Autor> {
        let autorAtual = await AutorModel.findById(id).exec();
        if (autorAtual != null) {
            autorAtual.primeiro_nome = autor.primeiro_nome;
            autorAtual.ultimo_nome = autor.ultimo_nome;
            return autorAtual.save();
        } else {
            throw new Error('Id inexistente');
        }
    }
}
